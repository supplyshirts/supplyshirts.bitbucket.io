function formValidation() {
    var name = document.registration.name;
    var address = document.registration.address;
    var email = document.registration.email;
    var password = document.registration.password;
    var phone = document.registration.phone;
    var dob = document.registration.dob;


    if (password_validation(password, 7, 12)) {
        if (allLetter(name)) {
            if (alphanumeric(address)) {
                if (ValidateDOB(dob)) {
                    if (allnumeric(phone)) {
                        if (ValidateEmail(email)) {
                        	return true;

                        }
                    }
                }
            }
        }
    }
    return false;
}

function password_validation(password, mx, my) {
    var password_len = password.value.length;
    if (password_len == 0 || password_len >= my || password_len < mx) {
        alert("Password should not be empty, or length be between " + mx + " to " + my);
        password.focus();
        return false;
    }
    return true;
}

function allLetter(name) {
    var letters = /^[A-Za-z ]+$/;
    if (name.value.match(letters)) {
        return true;
    } else {
        alert('Name must have alphabet characters only');
        name.focus();
        return false;
    }
}

function alphanumeric(address) {
    var letters = /^[a-zA-Z0-9\s,'-]*$/;
    if (address.value.match(letters)) {
        return true;
    } else {
        alert('Address must have alphanumeric characters only');
        address.focus();
        return false;
    }
}

function allnumeric(phone) {
    var numbers = /^[0-9]+$/;
    if (phone.value.match(numbers)) {
        return true;
    } else {
        alert('Phone number must have numeric characters only');
        phone.focus();
        return false;
    }
}

function ValidateEmail(email) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (email.value.match(mailformat)) {
        return true;
    } else {
        alert("You have entered an invalid email address");
        email.focus();
        return false;
    }
}

function ValidateDOB(dob) {
    var r = new Date();
    var dobcheck = dob.value.trim();
    var y = parseInt(dobcheck.substr(6, 4));
    var dobformat = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
    if (dob.value.match(dobformat)) {
        if ((parseInt(y) + 13) >= r.getFullYear()) {
            alert("Sorry, you are not allowed to register when you are under 13 years old.");
        } else if ((parseInt(y) + 50) <= r.getFullYear()) {
            alert("Please refer to our Help link for seniors.");
        } else {
            return true;
        }
    } else {
        alert("You have entered an invalid DOB, please input DD/MM/YYYY");
        dob.focus();
        return false;
    }
}