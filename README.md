## Project Sprint 2: Registration Form

---

## Project Brief:

https://drive.google.com/file/d/1nHhWAmzo2jFk_Cn_SZZNgcHJ_u5b4Ifu/view?usp=sharing

---

## Fields:


**Name**: required

**Address**: required

**Email**: required

**Password**: required

**Phone**: required

**DOB**: required


## JavaScript Validation on the fields:


**Name**: validate user inputs and accept Latin characters only.

**Address**: validate user inputs and accept Latin characters and numbers only.

**Email**: valid user inputs and accept valid type likes *email@com.com* only.

**Password**: valid user inputs and accept valid password from 7 to 12 characters only.

**Phone**: valid user inputs and accept numbers only.

**DOB**: valid user inputs and accept DD/MM/YYYY type, and will only allow users between 13 to 50 years old.

---

## Lighthouse results:

https://i.imgur.com/T6dxZZk.png

---